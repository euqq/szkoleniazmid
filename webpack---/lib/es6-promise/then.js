'use strict';

export default then;
import {
    invokeCallback,
    subscribe,
    FULFILLED,
    REJECTED,
    noop,
    makePromise,
    PROMISE_ID
} from './-internal';

import {
    asap
} from './asap';

function then(onFulfillment, onRejection) {
    var _arguments = arguments;

    var parent = this;

    var child = new this.constructor(noop);

    if (child[PROMISE_ID] === undefined) {
        makePromise(child);
    }

    var _state = parent._state;

    if (_state) {
        (function() {
            var callback = _arguments[_state - 1];
            asap(function() {
                return invokeCallback(_state, child, callback, parent._result);
            });
        })();
    } else {
        subscribe(parent, child, onFulfillment, onRejection);
    }

    return child;
}