'use strict';

export {
    objectOrFunction
};
export {
    isFunction
};
export {
    isMaybeThenable
};

function objectOrFunction(x) {
    return typeof x === 'function' || typeof x === 'object' && x !== null;
}

function isFunction(x) {
    return typeof x === 'function';
}

function isMaybeThenable(x) {
    return typeof x === 'object' && x !== null;
}

var _isArray = undefined;
if (!Array.isArray) {
    _isArray = function(x) {
        return Object.prototype.toString.call(x) === '[object Array]';
    };
} else {
    _isArray = Array.isArray;
}

var isArray = _isArray;
export {
    isArray
};